filetype off

"----------Configuration de Vundle----------"
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin('~/.vim/bundle')

"----------Bundle par défaut----------"
Plugin 'VundleVim/Vundle.vim'

"----------Bundle Perso----------"
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'kien/ctrlp.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'shawncplus/phpcomplete.vim'
Plugin 'tpope/vim-fugitive'

Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'sniphpets/sniphpets'
Plugin 'sniphpets/sniphpets-common'
Plugin 'sniphpets/sniphpets-postfix-codes'

call vundle#end()
set laststatus=2								"Nécessaire pour vim-airline


