Readme
=====================


For this project, I want to drop my vim config.
They will change with the time.

----------

Installation
--------------------------------------

Clone the projet where you want: 

> **Commande:** `git clone git@bitbucket.org:Ouite/myvimconfig.git myVimConfig`

> **Commande:** `cd myVimConfig`

> **Commande:** `cp -r ./.vim ~/`

> **Commande:** `cp -r ./.vimrc ~/`

> **Commande:** `git clone https://github.com/VundleVim/Vundle.vim.git Vundle.vim`

> **Commande:** `cp -r ./Vundle.vim ~/.vim/bundle/Vundle.vim`

> **Commande:** `vim +PluginInstall +qall`

