set nocompatible							"Dernière conf Vim réglages/options.
so ~/.vim/plugins.vim							"Import des plugins Vundle

filetype plugin indent on
syntax enable


"--------Plugins----------"


"let g:airline_powerline_fonts = 1

"/
"/ Bundle Airline
"/
let g:airline_extensions = ['branch']
let g:airline_powerline_fonts = 1
let g:airline_theme= "molokai"
let g:airline_inactive_collapse=1
let g:airline_mode_map = {
      \ '__' : '-',
      \ 'n'  : 'N',
      \ 'i'  : 'I',
      \ 'R'  : 'R',
      \ 'c'  : 'C',
      \ 'v'  : 'V',
      \ 'V'  : 'V',
      \ '' : 'V',
      \ 's'  : 'S',
      \ 'S'  : 'S',
      \ '' : 'S',
      \ }
let g:airline#extensions#branch#enabled = 0 
"let g:airline#extensions#branch#format = 1
let g:airline#extensions#branch#format = 0



"/
"/ NERDTreeToggle
"/
nmap <D-&> :NERDTreeToggle<CR>

"/
"/ Snippets 
"/
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-w>"

"/
"/ CtrlP
"/
nmap <c-r> :CtrlPBufTag<CR>
nmap <c-e> :CtrlPMRUFiles<CR>
if exists("g:ctrl_user_command")
  unlet g:ctrlp_user_command
endif
set wildignore+=*/vendor/*,*/app/cache/*,*/app/logs/*

"/
"/ Phpcomplete
"/
let g:phpcomplete_mappings = {
  \ 'jump_to_def': '<C-)>',
  \ }
let g:phpcomplete_relax_static_constraint = 1
let g:phpcomplete_complete_for_unknown_classes = 1
let g:phpcomplete_search_tags_for_variables = 1
let g:phpcomplete_min_num_of_chars_for_namespace_completion = 1
let g:phpcomplete_parse_docblock_comments = 1
let g:phpcomplete_cache_taglists = 1
let g:phpcomplete_enhance_jump_to_definition = 1

"----------Configuration generale----------"
let mapleader = ','
set backspace=indent,eol,start						"Fait un backspace comme sur les autres éditeur
set shiftwidth=4

set number
colorscheme atom-dark-256
set linespace=10
set guifont=Fira_Code:h15
set t_CO=256
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R
let g:molokai_original = 0
let g:rehash256 = 1


"----------Highlight----------"
set hlsearch
set incsearch
nmap ,<Space> :noh<CR>

"----------Split Management----------"
set splitbelow
set splitright


"----------Mappings----------"
"Permet d'éditer vimrc simplement
nmap <leader>ev :tabedit $MYVIMRC <cr> 
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
augroup END
